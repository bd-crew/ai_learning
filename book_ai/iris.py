from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd

iris_dataset = load_iris()

#전체 데이터의 일부분을 임의로 나누어 주어서 검증용 데이터 셋을 따로 나누어 주는 부분이다.
X_train, X_test, y_train, y_test = train_test_split(iris_dataset['data'],iris_dataset['target'],random_state=0)

print("X_train 크기 : {}".format(X_train.shape))
print("y_train 크기 : {}".format(y_train.shape))

print("X_test 크기 : {}".format(X_test.shape))
print("y_test 크기 : {}".format(y_test.shape))

#X_train 데이터를 사용해서 데이터 프레임을 만듭니다.
#열의 이름은 iris_dataset.feature_names에 있는 문자열을 사용합니다.
iris_df = pd.DataFrame(X_train, columns=iris_dataset.feature_names)

#데이터 프레임을 사용해서 y_train에 따라 색으로 구분된 산점도 행렬을 만듭니다.
maps = pd.plotting.scatter_matrix(iris_df, c=y_train, figsize=(15,15), marker='o', hist_kwds={'bins':20},
                           s = 60, alpha = .8)